STEP 1: Define ansible vault password that should be stored inside well protected file (that won't be pushed to this gitlab repository). For example, file could be called vaultpass. That pass will also be stored as a variable in Bitbucket in order to decrypt secret values during pipeline execution.

STEP 2: Encrypt secret string(s) using previosly defined vault password using command:

ansible-vault encrypt_string --vault-password-file vaultpass 'secret_string' --name 'secret'

where:
1) --vault-password-file argument should be provided with the path of vault password file and exact secret value, in that order, and
2) --name argument identifies key of the secret string (in our case, it's 'secret').


STEP 3: Output of previous command(s) should be kept inside vars.yaml file alongside other predefined secrets. Key 'Name' should be used to define purpose of mentioned secret keys.
